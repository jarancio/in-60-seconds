## Mapping the personal data ecosystem

@img[](https://pdio-public-daphne.s3.amazonaws.com/static/images/personaldata_chatbot_icon.original.png)

---?image=https://upload.wikimedia.org/wikipedia/commons/e/ee/Eso1524aArtist%E2%80%99s_impression_of_CR7_the_brightest_galaxy_in_the_early_Universe.jpg&opacity=40

### Example
### @color[white](Of background image)

---
@title[Customize Slide Layout]

@snap[west span-50]
## Some text, aligned left
@snapend

@snap[east span-50]
![](assets/img/presentation.png)
@snapend

---
## An example of boxed text

@box[bg-orange text-white rounded demo-box-pad](Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.)

---?color=black


@snap[north span-35]
@box[bg-green text-white demo-box-pad](1. Plan#Lorem ipsum dolor sit amet eiusmod)
@snapend

@snap[east span-35]
@box[bg-orange text-white rounded demo-box-pad](2. Build#Sed do eiusmod tempor labore)
@snapend

@snap[south span-35]
@box[bg-pink text-white demo-box-pad](3. Measure#Cupidatat non proident sunt in)
@snapend

@snap[west span-35]
@box[bg-blue text-white waved demo-box-pad](4. Repeat#Ut enim ad minim veniam prodient)
@snapend

@snap[midpoint]
@fa[refresh fa-3x]
@snapend

---?color=rebeccapurple

@quote[It's what I do that defines me.](Bruce Wayne)

---?image=assets/images/feedback.jpg&size=cover&opacity=20&color=#b7410e

<br>

## @color[white](Lists)

@ul[circles]
- Speaker Performance
- Product Evaluation
- Market Research
- User Ratings Poll
- Academic Research
- Customer Satisfaction
- And much  more...
@ulend

------?gist=onetapbeyond/8da53731fd54bab9d5c6&lang=Groovy&title=GIST: Groovy Snippet

[More here](https://gitpitch.com/gitpitch/gist-code-presenting)

